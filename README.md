# META

# Info on the project.

Color codes we use for themes and images.
- ![#8987DF](https://placehold.it/15/8987DF/000000?text=+) `#8987DF`
- ![#D0F7FF](https://placehold.it/15/D0F7FF/000000?text=+) `#D0F7FF`
- ![#E4EBFD](https://placehold.it/15/E4EBFD/000000?text=+) `#E4EBFD`
- ![#E4EBFD](https://placehold.it/15/FBFCFC/000000?text=+) `#FBFCFC`
- ![#8269D8](https://placehold.it/15/8269D8/000000?text=+) `#8269D8`

Storage of important files for community use.

# Important links.

- Discord

https://discord.egem.io

- Telegram

https://t.me/egemofficial

- Twitter

https://twitter.com/ETHERGEMCOIN

- Reddit

https://www.reddit.com/r/egem/

- Github/Gitlab

https://github.com/TeamEGEM - we moved all recent work to gitlab this is basically an archive.

https://gitlab.com/ethergem

- EXCHANGE INFO

http://egem.exchange/

- TRADE AND EXCHANGE

https://graviex.net/markets/egembtc?

https://graviex.net/markets/egemeth?

https://app.stex.com/en/trade/pair/BTC/EGEM/1D

- MARKET INFO

https://www.cheddur.com/coins/ethergem

https://coinmarketcap.com/currencies/ethergem/

https://www.livecoinwatch.com/price/EtherGem-EGEM

https://walletinvestor.com/currency/ethergem

https://www.worldcoinindex.com/coin/ethergem

https://coincodex.com/crypto/ethergem/

https://coinlib.io/coin/EGEM/EtherGem

- Mining Calculator

https://www.coincalculators.io/coin.aspx?crypto=ethergem-mining-calculator

https://mining.alekseirubin.com/calculator/EGEM
